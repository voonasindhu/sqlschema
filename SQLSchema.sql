USE [master]
GO
/****** Object:  Database [LinnTask]    Script Date: 12/12/2016 08:47:59 ******/
CREATE DATABASE [LinnTask]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LinnTask', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\LinnTask.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'LinnTask_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\LinnTask_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [LinnTask] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LinnTask].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LinnTask] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LinnTask] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LinnTask] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LinnTask] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LinnTask] SET ARITHABORT OFF 
GO
ALTER DATABASE [LinnTask] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LinnTask] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [LinnTask] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LinnTask] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LinnTask] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LinnTask] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LinnTask] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LinnTask] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LinnTask] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LinnTask] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LinnTask] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LinnTask] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LinnTask] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LinnTask] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LinnTask] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LinnTask] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LinnTask] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LinnTask] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LinnTask] SET RECOVERY FULL 
GO
ALTER DATABASE [LinnTask] SET  MULTI_USER 
GO
ALTER DATABASE [LinnTask] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LinnTask] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LinnTask] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LinnTask] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'LinnTask', N'ON'
GO
USE [LinnTask]
GO
/****** Object:  UserDefinedTableType [dbo].[ConsignmentType]    Script Date: 12/12/2016 08:47:59 ******/
CREATE TYPE [dbo].[ConsignmentType] AS TABLE(
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[PhoneNumber] [varchar](15) NULL,
	[CountryISO2] [varchar](50) NULL,
	[PostalCode] [varchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[ItemType]    Script Date: 12/12/2016 08:47:59 ******/
CREATE TYPE [dbo].[ItemType] AS TABLE(
	[ItemCode] [varchar](50) NULL,
	[Quantity] [varchar](50) NULL,
	[UnitWeight] [decimal](18, 0) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[packageType]    Script Date: 12/12/2016 08:47:59 ******/
CREATE TYPE [dbo].[packageType] AS TABLE(
	[PackageWidth] [decimal](18, 0) NULL,
	[PackageHeight] [decimal](18, 0) NULL,
	[PackageDepth] [varchar](50) NULL,
	[PackageType] [varchar](50) NULL
)
GO
/****** Object:  StoredProcedure [dbo].[spAddConsignment]    Script Date: 12/12/2016 08:48:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Sindhu V
-- Create date: 08/12/2016 20:34
-- Description:	Insert consignment list
-- =============================================
CREATE PROCEDURE [dbo].[spAddConsignment] 
   @Id int OUTPUT,
   @consignemnttable  ConsignmentType readonly,
   @packagetable  packageType readonly,
   @itemtable ItemType  readonly
	as
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRY
		BEGIN TRANSACTION

			-- Insert statements for procedure here
			INSERT INTO LinnTask.DBO.Consignment 
			([ConsignmentDate] ,
						[Address1] ,
						[Address2] ,
						[Address3] ,
						[City] ,
						[PhoneNumber] ,
						[CountryISO2] ,
						[PostalCode] 
				)
	
				SELECT  getdate() ,
						[Address1] ,
						[Address2] ,
						[Address3] ,
						[City] ,
						[PhoneNumber] ,
						[CountryISO2] ,
						[PostalCode] 
				FROM @consignemnttable
				DECLARE @ConsignmentId int =SCOPE_IDENTITY()

				set @Id = @ConsignmentId;

				INSERT INTO LinnTask.DBO.Package 
				([ConsignmentId] ,
				[PackageWidth] ,
				[PackageHeight] ,
				[PackageDepth] ,
				[PackageType] 
					)
	
					SELECT @ConsignmentId ,
				[PackageWidth] ,
				[PackageHeight] ,
				[PackageDepth] ,
				[PackageType]  
				FROM @packagetable

				DECLARE @PackageId int = SCOPE_IDENTITY()
		
				INSERT INTO LinnTask.DBO.Item 
				([PackageId] ,
				[ItemCode] ,
				[Quantity] ,
				[UnitWeight] 
					)
	
					SELECT @PackageId ,
				[ItemCode] ,
				[Quantity] ,
				[UnitWeight]
				FROM @itemtable
		
		 --Commits here
		 COMMIT
		 
	END TRY
	BEGIN CATCH
	   --Rollbacks the data if any single transaction fails above
	   ROLLBACK
	END CATCH
		

END

GO
/****** Object:  StoredProcedure [dbo].[spGetConsignment]    Script Date: 12/12/2016 08:48:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sindhura
-- Create date: 07/12/2016
-- Description:	Get Shipment details
-- =============================================
CREATE PROCEDURE [dbo].[spGetConsignment]  
	-- Add the parameters for the stored procedure here
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	WITH CTE (PackageId, PackageWeight)
	AS
	(
		SELECT p.PackageId, SUM(unitweight)
		FROM DBO.Package P
		INNER JOIN 
			 DBO.Item I ON P.PackageId = I.PackageId
		GROUP BY p.PackageId
	) 	
	SELECT con.Address1
		   ,con.Address2
		   ,con.Address3
		   ,con.City
		   ,con.CountryISO2
		   ,con.ConsignmentDate
		   ,con.PhoneNumber
		   ,con.ConsignmentDate
		   ,con.PostalCode
		   ,pac.PackageDepth
		   ,pac.PackageHeight
		   ,pac.PackageWidth
		   ,pac.PackageType
		   ,itm.ItemCode
		   ,itm.Quantity
		   ,itm.UnitWeight
	FROM Consignment AS CON 
	INNER JOIN 
		 Package AS PAC ON PAC.ConsignmentId = CON.ConsignmentId 
	INNER JOIN 
		 Item AS ITM ON ITM.PackageId = PAC.PackageId 
	INNER JOIN cte c on c.PackageId=pac.PackageId
	WHERE CON.ConsignmentId = @Id
	ORDER BY PackageWeight;
END

GO
/****** Object:  Table [dbo].[Consignment]    Script Date: 12/12/2016 08:48:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Consignment](
	[ConsignmentId] [int] IDENTITY(1,1) NOT NULL,
	[ConsignmentDate] [datetime] NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[CountryISO2] [varchar](50) NULL,
	[PostalCode] [varchar](50) NULL,
 CONSTRAINT [PK_Consignment1] PRIMARY KEY CLUSTERED 
(
	[ConsignmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Item]    Script Date: 12/12/2016 08:48:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Item](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[PackageId] [int] NULL,
	[ItemCode] [varchar](50) NULL,
	[Quantity] [varchar](50) NULL,
	[UnitWeight] [decimal](18, 0) NULL,
 CONSTRAINT [PK_Item] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Package]    Script Date: 12/12/2016 08:48:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Package](
	[PackageId] [int] IDENTITY(1,1) NOT NULL,
	[ConsignmentId] [int] NOT NULL,
	[PackageWidth] [decimal](18, 0) NULL,
	[PackageHeight] [decimal](18, 0) NULL,
	[PackageDepth] [varchar](50) NULL,
	[PackageType] [varchar](50) NULL,
 CONSTRAINT [PK_Package] PRIMARY KEY CLUSTERED 
(
	[PackageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Consignment] ON 



INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (12, CAST(0x0000A6D601572841 AS DateTime), N'8', N'Barnes Wallis Road', N'Arena', N'Fareham', N'123456789', N'123456', N'ABCDEF')
INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (13, CAST(0x0000A6D6015CFC16 AS DateTime), N'9', N'Barnes Wallis Road', N'Arena', N'Fareham', N'0123456789', N'123456', N'ABCDEF')
INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (14, CAST(0x0000A6D6015D1BE9 AS DateTime), N'10', N'Barnes Wallis Road', N'Arena', N'Fareham', N'0123456789', N'123456', N'ABCDEF')
INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (15, CAST(0x0000A6D7009F719A AS DateTime), N'11', N'Barnes Wallis Road', N'Arena', N'Fareham', N'0123456789', N'123456', N'ABCDEF')
INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (16, CAST(0x0000A6D700AA4FEC AS DateTime), N'12', N'Barnes Wallis Road', N'Arena', N'Fareham', N'0123456789', N'123456', N'ABCDEF')
INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (17, CAST(0x0000A6D700AA5F63 AS DateTime), N'13', N'Barnes Wallis Road', N'Arena', N'Fareham', N'0123456789', N'123456', N'ABCDEF')
INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (18, CAST(0x0000A6D700AA8D05 AS DateTime), N'14', N'Barnes Wallis Road', N'Arena', N'Fareham', N'0123456789', N'123456', N'ABCDEF')
INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (19, CAST(0x0000A6D700AB07E2 AS DateTime), N'15', N'Barnes Wallis Road', N'Arena', N'Fareham', N'0123456789', N'123456', N'ABCDEF')
INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (20, CAST(0x0000A6D700AB97C5 AS DateTime), N'8', N'Barnes Wallis Road', N'Arena', N'Fareham', N'0123456789', N'123456', N'ABCDEF')
INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (21, CAST(0x0000A6D700ABBACF AS DateTime), N'9', N'Barnes Wallis Road', N'Arena', N'Fareham', N'0123456789', N'123456', N'ABCDEF')
INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (22, CAST(0x0000A6D700ABD7C3 AS DateTime), N'10', N'Barnes Wallis Road', N'Arena', N'Fareham', N'0123456789', N'123456', N'ABCDEF')
INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (23, CAST(0x0000A6D700ABEF31 AS DateTime), N'11', N'Barnes Wallis Road', N'Arena', N'Fareham', N'0123456789', N'123456', N'ABCDEF')
INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (24, CAST(0x0000A6D700AC4E64 AS DateTime), N'12', N'Barnes Wallis Road', N'Arena', N'Fareham', N'0123456789', N'123456', N'ABCDEF')
INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (25, CAST(0x0000A6D700AC7322 AS DateTime), N'13', N'Barnes Wallis Road', N'Arena', N'Fareham', N'0123456789', N'123456', N'ABCDEF')
INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (26, CAST(0x0000A6D900E4D9C0 AS DateTime), N'14', N'Barnes Wallis Road', N'Arena', N'Fareham', N'0123456789', N'123456', N'ABCDEF')
INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (27, CAST(0x0000A6D900E58BB4 AS DateTime), N'15', N'Barnes Wallis Road', N'Arena', N'Fareham', N'0123456789', N'123456', N'ABCDEF')
INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (28, CAST(0x0000A6D9017AE553 AS DateTime), N'16', N'Barnes Wallis Road', N'Arena', N'Fareham', N'0123456789', N'123456', N'ABCDEF')
INSERT [dbo].[Consignment] ([ConsignmentId], [ConsignmentDate], [Address1], [Address2], [Address3], [City], [PhoneNumber], [CountryISO2], [PostalCode]) VALUES (29, CAST(0x0000A6D901810E3A AS DateTime), N'8', N'Barnes Wallis Road', N'Arena', N'Fareham', N'0123456789', N'123456', N'ABCDEF')
SET IDENTITY_INSERT [dbo].[Consignment] OFF
SET IDENTITY_INSERT [dbo].[Item] ON 

INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (1, 6, N'abcd', N'2', CAST(10 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (2, 6, N'123', N'2', CAST(10 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (3, 7, N'Tester0', N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (4, 7, N'Tester1', N'1', CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (5, 8, N'Tester0', N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (6, 8, N'Tester1', N'1', CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (7, 9, N'Tester0', N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (8, 9, N'Tester1', N'1', CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (9, 10, N'Tester0', N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (10, 10, N'Tester1', N'1', CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (11, 11, N'Tester0', N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (12, 11, N'Tester1', N'1', CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (13, 12, N'Tester0', N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (14, 12, N'Tester1', N'1', CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (15, 13, N'Tester0', N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (16, 13, N'Tester1', N'1', CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (17, 14, N'Tester0', N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (18, 14, N'Tester1', N'1', CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (19, 15, N'Tester0', N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (20, 15, N'Tester1', N'1', CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (21, 16, N'Tester0', N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (22, 16, N'Tester1', N'1', CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (23, 17, N'Tester0', N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (24, 17, N'Tester1', N'1', CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (25, 18, N'Tester0', N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (26, 18, N'Tester1', N'1', CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (27, 19, N'Tester0', N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (28, 19, N'Tester1', N'1', CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (29, 20, N'Tester0', N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (30, 20, N'Tester1', N'1', CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (31, 21, N'Tester0', N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (32, 21, N'Tester1', N'1', CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (33, 22, N'Tester0', N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (34, 22, N'Tester1', N'1', CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (35, 23, N'Tester0', N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Item] ([ItemId], [PackageId], [ItemCode], [Quantity], [UnitWeight]) VALUES (36, 23, N'Tester1', N'1', CAST(100 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[Item] OFF
SET IDENTITY_INSERT [dbo].[Package] ON 



INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (6, 12, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (7, 13, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (8, 14, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (9, 15, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (10, 16, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (11, 17, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (12, 18, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (13, 19, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (14, 20, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (15, 21, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (16, 22, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (17, 23, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (18, 24, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (19, 25, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (20, 26, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (21, 27, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (22, 28, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
INSERT [dbo].[Package] ([PackageId], [ConsignmentId], [PackageWidth], [PackageHeight], [PackageDepth], [PackageType]) VALUES (23, 29, CAST(3 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)), N'ABCD', N'ABCD')
SET IDENTITY_INSERT [dbo].[Package] OFF
/****** Object:  Index [NonClusteredIndex_PackageId-20161208-220115]    Script Date: 12/12/2016 08:48:00 ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex_PackageId-20161208-220115] ON [dbo].[Item]
(
	[PackageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Package] FOREIGN KEY([PackageId])
REFERENCES [dbo].[Package] ([PackageId])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Package]
GO
ALTER TABLE [dbo].[Package]  WITH CHECK ADD  CONSTRAINT [FK_Package_Consignment] FOREIGN KEY([ConsignmentId])
REFERENCES [dbo].[Consignment] ([ConsignmentId])
GO
ALTER TABLE [dbo].[Package] CHECK CONSTRAINT [FK_Package_Consignment]
GO
USE [master]
GO
ALTER DATABASE [LinnTask] SET  READ_WRITE 
GO
